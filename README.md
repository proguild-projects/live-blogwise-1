# Blogwise Project

This is a demonstration of full-stack web development. There is A LOT of things to consider when building a web app. Blogwise is designed to be a personal blog that is as minimal as possible for our author to use. Our author needs to be able to:

1. Add blog posts.
2. Edit blog posts

Just these 2 features entails a lot of work as you'll soon find out. To develop a full web app you need to manage the following things:

- Data
- HTML/CSS pages
- Application logic

These are 3 broad areas of a web application. This pattern is also known as MVC: model, view, controller.

## Tools

Blogwise uses as few third-party modules as possible. You can read the requirements file, but the things we'll need to install are:

- Flask
- Flask-SQLAlchemy
- psycopg2

We may install some additional dev tools, but they are entirely optional.


## Development Goal Posts

Development is messy. This neat list is good for organizing our thoughts and drafting a plan. But the process of building the app won't be as neat and clean!

1. Minimum Viable Product (MVP): A prototype of the basic app and its basic functionality. This is where we figure out what features to develop and outline how they will work.
2. v1.0: Using tests, develop the first release version of the app
3. Refactoring: A clean-up operation to refine the core app to make it extensible for future upgrades.


### Notes

**Create Files**
Readme, gitignore of course. Create app module with views, models and main (server).
Install basics: `flask, flask-debugtoolbar`

**Start App**
Write first unit test for 200 on homepage. This gets the app config setup in a solid place so that we dont have to worry about defining config after this.

Build the app with the dev server running at all times. Follow the errors until status 200 for home page only.

This will create `create_app` function for starting the app. Name `app.py` or `wsgi.py`.

**Homepage**
Construct basic structure and layout of the homepage. Import styles, setup static, create blocks for title and content.

Set template folder and basic templates with necessary partials (don't code ahead).

**Data**
Setup data management. Cant display articles without articles. Build model, connect DB, display data on homepage from DB.

**Views**
Bare bones list and details view.
