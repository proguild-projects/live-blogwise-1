from flask import Flask

from blogwise.views import app


def create_app(mod_path: str = 'blogwise.settings'):
    curr_app = app or Flask(__name__)
    curr_app.config.from_object(mod_path)

    return curr_app

