from blogwise import create_app
from flask_debugtoolbar import DebugToolbarExtension


# app.secret_key = 'secretzzz'
app = create_app()
DebugToolbarExtension(app)

if __name__ == '__main__':
    app.run()

