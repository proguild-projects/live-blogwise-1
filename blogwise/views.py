from flask import Flask, render_template
from blogwise.models import Article

app = Flask(__name__)


@app.route('/')
def index():
    articles = Article.get_all()
    return render_template('index.html')
