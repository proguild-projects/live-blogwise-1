import sqlite3
import uuid
from datetime import datetime

db = sqlite3.connect('blogwise.sqlite3')


class Article:
    table_name: str = 'articles'
    connection = db
    db.row_factory = sqlite3.Row
    date_format: str = '%m-%d-%Y %H:%M:%S'

    def __init__(self, **kwargs):
        self.id: str = str(uuid.uuid4())
        self.title: str
        self.content: str
        self._published: datetime = datetime.now()
        # self._published: str = datetime.now().strftime(self.date_format)

    @classmethod
    def get_all(cls):
        statement = f"SELECT * FROM {cls.table_name}"
        return cls.connection.execute(statement).fetchall()

    @classmethod
    def create(cls, **kwargs):
        with cls.connection:
            cls.connection.execute('CREATE TABLE IF NOT EXISTS articles(id, title, content, published)')
            statement = f'INSERT INTO {cls.table_name} VALUES (:title, :content, :published)'
            return cls.connection.execute(statement, kwargs)


def connect():
    db.execute("CREATE TABLE articles(title, year, author) IF NOT EXISTS")


def seed():
    for i in range(100):
        article = Article.create()


if __name__ == '__main__':
    print('Models ready.')
