import unittest
from http import HTTPStatus
from blogwise import create_app
from blogwise.models import Article


class BlogwiseViewsTestCase(unittest.TestCase):
    def setUp(self):
        self.app = create_app()
        self.client = self.app.test_client()

    def test_homepage_200(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, HTTPStatus.OK)
        self.assertIn(b'Blogwise', response.data)


class BlogwiseArticlesTestCase(unittest.TestCase):
    def test_get_all_articles(self):
        Article.create()
        Article.create()
        articles = Article.get_all()
        self.assertEqual(len(articles), 2)


if __name__ == '__main__':
    unittest.main()
